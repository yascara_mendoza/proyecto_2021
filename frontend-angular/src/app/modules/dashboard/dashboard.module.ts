import { NgModule } from '@angular/core';
import { HomeDasboardComponent } from './home-dasboard/home-dasboard.component';
import { NavbarDashboardComponent } from './navbar-dashboard/navbar-dashboard.component';
import { SidebarDashboardComponent } from './sidebar-dashboard/sidebar-dashboard.component';
import { SkeletonDashboardComponent } from './skeleton-dashboard/skeleton-dashboard.component';
import { SharedModule } from '@shared/shared.module';
import { DashboarRoutingModule } from './dashboar-routing.module';



@NgModule({
  declarations: [
    HomeDasboardComponent,
    NavbarDashboardComponent,
    SidebarDashboardComponent,
    SkeletonDashboardComponent,
  
  ],
  imports: [
    SharedModule,
    DashboarRoutingModule
  ]
})
export class DashboardModule { }
