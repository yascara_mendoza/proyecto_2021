import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeDasboardComponent } from './home-dasboard.component';

describe('HomeDasboardComponent', () => {
  let component: HomeDasboardComponent;
  let fixture: ComponentFixture<HomeDasboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeDasboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeDasboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
