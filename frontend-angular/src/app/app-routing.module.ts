import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SkeletonComponent } from '@layout/skeleton/skeleton.component';
import { SkeletonDashboardComponent } from '@modules/dashboard/skeleton-dashboard/skeleton-dashboard.component';


const routes: Routes = [
{
  path:'',
  component: SkeletonComponent,
  children:[
    {
      path:'',
      loadChildren:()=>
      import('@modules/home/home.module').then((m)=>m.HomeModule)
    },
    {
      path:'auth',
      loadChildren:()=>
      import('@modules/auth/auth.module').then((m)=>m.AuthModule)
    }
  ]
},
{
  path:'dashboard',
  component: SkeletonDashboardComponent,
  children:[
    {
      path: 'home',
      loadChildren:()=>
      import('@modules/dashboard/dashboard.module').then((m)=>m.DashboardModule)
    },
    {
      path: 'discover',
      loadChildren:()=>
      import('@modules/discover/discover.module').then((m)=>m.DiscoverModule)
    }
    
  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
