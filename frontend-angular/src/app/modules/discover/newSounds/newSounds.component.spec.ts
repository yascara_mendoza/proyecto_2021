import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewSoundsComponent } from './newSounds.component';

describe('NewSoundsComponent', () => {
  let component: NewSoundsComponent;
  let fixture: ComponentFixture<NewSoundsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewSoundsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewSoundsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
