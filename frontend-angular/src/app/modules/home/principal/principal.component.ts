import { Component, OnInit } from '@angular/core';
import { LoadScriptsService } from "./../../../load-scripts.service";

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

  constructor( private _LoadScripts: LoadScriptsService) 
  { 
    _LoadScripts.Load(["main"]);
  }

  ngOnInit(): void {
  }

}
