import { NgModule } from '@angular/core';

import { PreferencesComponent } from './preferences/preferences.component';
import { RecommendedComponent } from './recommended/recommended.component';
import { TrendComponent } from './trend/trend.component';
import { NewSoundsComponent } from './newSounds/newSounds.component';
import { AlbumsComponent } from './albums/albums.component';
import { ArtistsComponent } from './artists/artists.component';
import { BooksComponent } from './books/books.component';
import { SharedModule } from '@shared/shared.module';
import { DiscoverRoutingModule } from './discover-routing.module';



@NgModule({
  declarations: [
    PreferencesComponent,
    RecommendedComponent,
    TrendComponent,
    NewSoundsComponent,
    AlbumsComponent,
    ArtistsComponent,
    BooksComponent
  ],
  imports: [
    SharedModule,
    DiscoverRoutingModule
  ]
})
export class DiscoverModule { }
