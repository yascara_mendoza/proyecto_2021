import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlbumsComponent } from './albums/albums.component';
import { ArtistsComponent } from './artists/artists.component';
import { BooksComponent } from './books/books.component';
import { NewSoundsComponent } from './newSounds/newSounds.component';
import { PreferencesComponent } from './preferences/preferences.component';
import { RecommendedComponent } from './recommended/recommended.component';
import { TrendComponent } from './trend/trend.component';



const routes: Routes = [
  {
      path:'preferences',
    component:PreferencesComponent
  },
  {
    path:'recommended',
   component:RecommendedComponent
  },
  {
    path:'trend',
 component:TrendComponent
  },
  {
    path:'newSounds',
  component:NewSoundsComponent
  },
  {
    path:'albums',
  component:AlbumsComponent
  },
  {
    path:'artists',
  component:ArtistsComponent
  },
  {
    path:'books',
  component:BooksComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiscoverRoutingModule { }
