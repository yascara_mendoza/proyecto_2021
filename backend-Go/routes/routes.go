package routes

import (
	controller "restapi/controllers"

	"github.com/gorilla/mux"
)

//* devuelve valor a la variable
func Setup() *mux.Router {
	route := mux.NewRouter().StrictSlash(true)
	route.HandleFunc("/api/index", controller.Index)
	return route
}
