import { Component, OnInit } from '@angular/core';
import { LoadScriptsService } from './../../../load-scripts.service';

@Component({
  selector: 'app-preferences',
  templateUrl: './preferences.component.html',
  styleUrls: ['./preferences.component.css']
})
export class PreferencesComponent implements OnInit {

  constructor( private _LoadScripts: LoadScriptsService) 
  { 
    _LoadScripts.Load(["explorar"]);
  }

  ngOnInit(): void {
  }

}
