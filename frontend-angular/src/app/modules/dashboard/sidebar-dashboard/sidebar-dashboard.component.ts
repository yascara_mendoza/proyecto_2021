import { Component, OnInit } from '@angular/core';
import { LoadScriptsService } from './../../../load-scripts.service';

@Component({
  selector: 'app-sidebar-dashboard',
  templateUrl: './sidebar-dashboard.component.html',
  styleUrls: ['./sidebar-dashboard.component.css']
})
export class SidebarDashboardComponent implements OnInit {
  
  constructor( private _LoadScripts: LoadScriptsService) 
  { 
    _LoadScripts.Load(["explorar"]);
  }
  ngOnInit(): void {
  }

}
