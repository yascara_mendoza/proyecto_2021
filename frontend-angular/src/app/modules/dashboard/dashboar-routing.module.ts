import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeDasboardComponent } from './home-dasboard/home-dasboard.component';




const routes: Routes = [
    {
        path:'',
     component:HomeDasboardComponent
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboarRoutingModule { }
