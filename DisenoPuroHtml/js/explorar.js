const audio = document.getElementById('audio');
const playPause =document.getElementById('play');
const contenedor = document.querySelector('#contenedor');

document.querySelector('#boton-menu').addEventListener('click', () => {
	contenedor.classList.toggle('active');
});

const comprobarAncho = () => {
	if(window.innerWidth <= 768){
		contenedor.classList.remove('active');
	} else {
		contenedor.classList.add('active');
	}
}

comprobarAncho();

window.addEventListener('resize', () => {
	comprobarAncho();
});



playPause.addEventListener("click", () =>{
  if(audio.paused || audio.ended){
    playPause.querySelector(".botonPausa").classList.toggle("hide")
    playPause.querySelector(".botonPlay").classList.toggle("hide")
    audio.play();
  }
  else{
    audio.pause();
    playPause.querySelector(".botonPausa").classList.toggle("hide")
    playPause.querySelector(".botonPlay").classList.toggle("hide")
  }
})
