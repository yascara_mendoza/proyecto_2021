import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FooterComponent } from '@layout/footer/footer.component';
import { NavigationComponent } from '@layout/navigation/navigation.component';
import { SkeletonComponent } from '@layout/skeleton/skeleton.component';
// inicio servicio
import {LoadScriptsService} from "./load-scripts.service";
//fin servicio

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    NavigationComponent,
    SkeletonComponent,
  ],
  imports: [   // no se coloca datamodule porque no se necesita que se carge con todo el sistema
    CoreModule,
    SharedModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
  LoadScriptsService,
  {
  provide: LocationStrategy, // quita # de la URL del sistema
  useClass:PathLocationStrategy,
  }
],
  bootstrap: [AppComponent]
})
export class AppModule { }
