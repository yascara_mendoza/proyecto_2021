import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LoadScriptsService } from "./../../../load-scripts.service";
@Component({
  selector: 'app-skeleton-dashboard',
  templateUrl: './skeleton-dashboard.component.html',
  styleUrls: ['./skeleton-dashboard.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SkeletonDashboardComponent implements OnInit {

  constructor( private _LoadScripts: LoadScriptsService) 
  { 
    _LoadScripts.Load(["explorar"]);
  }

  ngOnInit(): void {
  }



}
